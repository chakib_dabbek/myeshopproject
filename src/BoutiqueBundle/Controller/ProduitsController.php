<?php

namespace BoutiqueBundle\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\Produits;
use WebBundle\Entity\Utilisateurs;
use WebBundle\Form\ProduitsType;

/**
 * Produits controller.
 *
 * @Route("/back_produits")
 */
class ProduitsController extends Controller
{
    /**
     * Lists all Produits entities.
     *
     * @Route("/", name="produits_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="produits_index_paginated")
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $boutique= $em->getRepository('WebBundle:Boutique')->findOneBy( array('responsable' => $this->getUser()));

        $query = $em->getRepository('WebBundle:Produits')->findBy( array('boutique' => $boutique));
        $paginator = $this->get('knp_paginator');
        $produits = $paginator->paginate(
            $query, $page, 9
        //Produits::NUM_ITEMS
        );
        $produits->setUsedRoute('produits_index_paginated');
        return $this->render('@Boutique/produits/index.html.twig', array(
            'produits' => $produits,
        ));
    }

    /**
     * Creates a new Produits entity.
     *
     * @Route("/new", name="produits_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();
        $boutique= $em->getRepository('WebBundle:Boutique')->findOneBy( array('responsable' => $this->getUser()));

        $produit = new Produits();
        $form = $this->createForm('BoutiqueBundle\Form\ProduitsType', $produit)
        ->remove('nb_achat');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $boutique= $em->getRepository('WebBundle:Boutique')->findOneBy( array('responsable' => $this->getUser()));
            $produit->setNbAchat(0);
            $produit->setBoutique($boutique);
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('produits_index');
        }
        return $this->render('@Boutique/produits/new.html.twig', array(
            'produit' => $produit,
            'boutique' =>$boutique,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing Produits entity.
     *
     * @Route("/{id}/edit", name="produits_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Produits $produit)
    {
        $em = $this->getDoctrine()->getManager();
        $user=$this->getUser();
        $boutique= $em->getRepository('WebBundle:Boutique')->findOneBy( array('responsable' => $this->getUser()));
        $editForm = $this->createForm('BoutiqueBundle\Form\ProduitsType', $produit)
        ->remove('nb_achat');

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('produits_index');
        }

        return $this->render('@Boutique/produits/edit.html.twig', array(
            'produit' => $produit,
            'boutique' =>$boutique,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     *
     * @Route("/search", name="rechercheproduit", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="produits_index_paginated")
     * @Method("GET")
     */
    public function rechercheAction(Request $request,$page){

        $em = $this->getDoctrine()->getManager();
        $boutique= $em->getRepository('WebBundle:Boutique')->findOneBy( array('responsable' => $this->getUser()));

        if(strlen($request->get('recherche'))>0){
            $repo= $em->getRepository('WebBundle:Produits');
            $query = $repo->createQueryBuilder('p')
                ->where('p.boutique = :boutique')
                ->andWhere('p.nom LIKE :search')
                ->setParameter('search', '%'.$request->get('recherche').'%')
                ->setParameter('boutique', $boutique)
                ->getQuery()
                ->getResult();

        }
        else{
            $query = $em->getRepository('WebBundle:Produits')->findBy( array('boutique' => $boutique));
        }
        $paginator = $this->get('knp_paginator');
        $produits = $paginator->paginate(
            $query, $page, 9
        //Produits::NUM_ITEMS
        );
        $produits->setUsedRoute('produits_index_paginated');
        return $this->render('@Boutique/produits/index.html.twig', array(
            'produits' => $produits,
        ));
    }

    /**
     * Deletes a Produits entity.
     *
     * @Route("/del/{id}", name="produits_delete")
     */
    public function deleteAction(Request $request, Produits $produit)
    {

            $em = $this->getDoctrine()->getManager();
            $em->remove($produit);
            $em->flush();
            $this->addFlash('success', 'Produit efface avec succes');
            $produits = $em->getRepository('WebBundle:Produits')->findAll();

        return $this->redirectToRoute('produits_index');
    }

    /**
     * Creates a form to delete a Produits entity.
     *
     * @param Produits $produit The Produits entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Produits $produit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('produits_delete', array('id' => $produit->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
