<?php

namespace BoutiqueBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

/**
 * {@inheritDoc}
 */
class SecurityController extends BaseController
{

    /**
     * {@inheritDoc}
     */
    public function renderLogin(array $data)
    {

        $requestAttributes = $this->container->get('request')->attributes;
        $authChecker = $this->container->get('security.authorization_checker');
        $this->container->get('session')->getFlashBag()->clear();

        if ('boutique_login' === $requestAttributes->get('_route')) {
            if ($authChecker->isGranted('ROLE_GESTIONNAIRE')) {
                $template = sprintf('BoutiqueBundle:Default:index.html.twig');
            }
            else {
                $template = sprintf('BoutiqueBundle:Security:login.html.twig');
            }
        } else {
            $template = sprintf('FOSUserBundle:Security:login.html.twig');
        }

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
