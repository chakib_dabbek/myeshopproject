<?php

namespace BoutiqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Boutique;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class DefaultController extends Controller
{

    /**
     * @Route("/", name="index")
     */
    public function IndexAction()
    {    
        
        return $this->render('BoutiqueBundle:Default:index.html.twig');
    }
    
    /**
     * @Route("/dashbord", name="boutique_backend")
     */
    public function dashbordAction()
    {
        $cj=0;$cs=0;$cm=0;$ca=0;
        $now=new \DateTime("now");
        $date=new \DateTime("now");
        $date=$date->add(\DateInterval::createFromDateString('-7 days'));
     //   dump($now);die;
        $user= $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $boutique= $em->getRepository('WebBundle:Boutique')->findOneBy( array('responsable' => $user));
        $query = $em->getRepository('WebBundle:Commande_Produit')->GetCommande($boutique->getId());
        //dump($query);die;

        foreach ($query as $c) {
           // dump($now);die;
            if ($c->getDeliveryDate() and $c->getDeliveryDate()->format("Y-m-d") == $now->format("Y-m-d")) {
                $cj = $cj+1;
            }
            if ($c->getDeliveryDate() and $c->getDeliveryDate()->format("Y-m") == $now->format("Y-m")) {
                $cm = $cm+1;
            }
            if ($c->getDeliveryDate() and $c->getDeliveryDate()->format("Y") == $now->format("Y")) {
                $ca = $ca+1;
            }
            if (($c->getDeliveryDate() and $c->getDeliveryDate()->format("Y-m-d") <= $now->format("Y-m-d")) and ( $date->format("Y-m-d")<= $c->getDeliveryDate()->format("Y-m-d"))  ) {
                $cs = $cs+1;
            }

        }

        //dump($cj.'/'.$cm.'/'.$ca.'/'.$cs);die;

        return $this->render('BoutiqueBundle:dashbord:dashbord.html.twig',array(
            'cj'=>$cj,
            'cs'=>$cs,
            'cm'=>$cm,
            'ca'=>$ca,

        ));
    }

}
