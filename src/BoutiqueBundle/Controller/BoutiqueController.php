<?php

namespace BoutiqueBundle\Controller;

use FOS\UserBundle\Mailer\TwigSwiftMailer;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Boutique;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use WebBundle\Entity\Utilisateurs;

/**
 * Produits controller.
 *
 * @Route("/back_boutique")
 */
class BoutiqueController extends Controller
{
    /**
     *
     * @Route("/", name="boutique_profile")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $boutique = $em->getRepository('WebBundle:Boutique')->findOneBy(array('responsable' => $user));
        return $this->render('@Boutique/boutique/index.html.twig', array(
            'boutique' => $boutique,
            'user' => $user

        ));
    }

    /**
     * Displays a form to edit an existing Produits entity.
     *
     * @Route("/{id}/edit", name="edit_profile")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Boutique $boutique)
    {
        $editForm = $this->createForm('BoutiqueBundle\Form\BoutiqueType', $boutique)
            ->remove('secteur')
            ->remove('email')
            ->remove('couleur');
        $editForm->handleRequest($request);
        $validator = $this->get('validator');
        $errors = $validator->validate($boutique);

      if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->merge($boutique);
            $em->flush();
            return $this->redirectToRoute('boutique_profile');
        }

        return $this->render('@Boutique/boutique/edit.html.twig', array(
            '$boutique' => $boutique,
            'edit_form' => $editForm->createView(),
            'errors' =>  $errors
        ));
    }

    /**
     * @Route("register/{boutique}", name="registerresp")
     */
    public function RegistrationAction($boutique)
    {
        return $this->render('BoutiqueBundle::registre.html.twig');
    }
    /**
     * @Route("password", name="modifpassword")
     * @Method({"GET", "POST"})
     */
    public function ModifPasswordAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $ancien = $this->get('security.password_encoder')
            ->encodePassword($user, $request->get('pwda'));

        if (strlen($request->get('pwdn')) > 0) {

            if ($user->getPassword() == $ancien) {
                    $nouveau = $this->get('security.password_encoder')
                        ->encodePassword($user, $request->get('pwdn'));
                    $user->setPassword($nouveau);
                    $em->persist($user);
                    $em->flush();
                    return $this->redirectToRoute('boutique_logout');
            } else {
                $message = \Swift_Message::newInstance()
                    ->setSubject('modification mot de passe')
                    ->setFrom(array('fatma.boussaid@gmail.com' => "E_SHOP"))
                    ->setTo($user->getEmail())
                    ->setCharset('utf-8')
                    ->setContentType('text/html')
                    ->setBody('verifier votre compte');
                $this->get('mailer')->send($message);
                return $this->redirectToRoute('boutique_logout');
            }
        } else {

        }
    }
}
