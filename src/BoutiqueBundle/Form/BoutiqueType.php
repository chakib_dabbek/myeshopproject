<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BoutiqueBundle\Form;

use Imagine\Image\Palette\Color\CMYK;
use Imagine\Image\Palette\Color\ColorInterface;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\CoreBundle\Form\Type\ColorSelectorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Email;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class BoutiqueType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // see http://symfony.com/doc/current/reference/forms/types.html
        // $builder->add('title', null, array('required' => false, ...));

        $builder
            ->add('raisonsociale',null, array(
                'required'   => true,
                'attr' => array('autofocus' => true,
                    'placeholder'=> 'raison sociale...',
                ),
                'label' => 'Raison Sociale',
            ))
            ->add('nomdomaine', null, array(
                'attr' => array('autofocus' => true,
                    'placeholder'=> 'nom domaine...',
                ),
                'label' => 'Nom Domaine ',
            ))
            ->add('email', 'Symfony\Component\Form\Extension\Core\Type\EmailType', array(
                'required'   => true,
                'attr' => array('autofocus' => true,
                    'placeholder'=> 'email...',),
                'label' => 'Email ',
            ))
            ->add('description', 'Symfony\Component\Form\Extension\Core\Type\TextareaType',
                array('label' => 'Description',
                    'attr'=> array( 'placeholder'=> 'description...',
                    )

                ))
            ->add('tel', null, array(
                'attr' => array('autofocus' => true,
                    'placeholder'=> 'telephone...',),
                'label' => 'Telephone ',
            ))

            ->add('adresse', null, array(
                'attr' => array('autofocus' => true,
                    'placeholder'=> 'adresse,ville,pays..',
                ),
                'label' => 'Adresse ',
            ))
            ->add('rib', null, array(
                'attr' => array('autofocus' => true,
                    'placeholder'=> 'rib...',),
                'label' => 'Rib ',
            ))
            ->add('matricule', null, array(
                'attr' => array('autofocus' => true,
                    'placeholder'=> 'matricule fiscale ...',
                ),
                'label' => ' Matricule Fiscale ',
            ))
            ->add('registre', null, array(
                'attr' => array('autofocus' => true,
                    'placeholder'=> ' registre de commerce ...',
                ),
                'label' => ' Registre De Commerce  ',
            ))
            ->add('couleur', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class,
                array('choices' => array(
                    'skin-1.css',
                    'skin-8.css',
                    'skin-2.css',
                    'skin-3.css',
                    'skin-4.css',
                    'skin-5.css',
                    'skin-7.css',
                    'skin-9.css',
                    'skin-10.css',
                    'skin-11.css',
                    'skin-6.css',
                ),
                    'choices_as_values' => true,
                    'choice_label' => function () { return false; },
                    'multiple'=>false,'expanded'=>true))







                ->add('secteur', EntityType::class, array(
                    'required'   => true,
                    'class' => 'WebBundle\Entity\SecteurActivity',
                    'choice_label' => 'getNom',
                ))
                ->add('logoFile', VichImageType::class, array(
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))


        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebBundle\Entity\Boutique',
            'cascade_validation' => true,
        ));
    }

}
