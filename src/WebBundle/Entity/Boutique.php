<?php

namespace WebBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use WebBundle\Entity\SecteurActivity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * boutique
 *
 * @ORM\Table("boutique")
 * @ORM\Entity(repositoryClass="WebBundle\Repository\BoutiqueRepository")
 * @UniqueEntity(
 *     fields={"raisonsociale"},
 *     errorPath="raisonsociale",
 *     message="cette raison sociale est deja utilisé")
 * @UniqueEntity(
 *     fields={ "email"},
 *     errorPath="email",
 *     message="cette  email est deja utilisé")
 * @Vich\Uploadable
 */
class Boutique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="raison_sociale", type="string", unique=true, length=125)
     */
    private $raisonsociale;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_domaine", type="string", length=125, nullable=true, unique=true)
     */
    private $nomdomaine;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=125, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="integer", nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="string", length=125, nullable=true)
     */
    private $couleur;
    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=250, nullable=true)
     */
    private $adresse;
     /**
     *
     * @ORM\Column(name="statut", type="boolean" ,options={"default":true})
     */
    private $status;
    /**
     * @var string
     *
     * @ORM\Column(name="rib", type="integer", nullable=true)
     */
    private $rib;
    /**
     * @var string
     *
     * @ORM\Column(name="matricule", type="string", length=250, nullable=true)
     */
    private $matricule;
    /**
     * @var string
     *
     * @ORM\Column(name="registre", type="string", length=250, nullable=true)
     */
    private $registre;

    /**
     *
     *
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo ;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="logo_images", fileNameProperty="logo")
     *
     * @var File
     */
    private $logoFile;

    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\SecteurActivity", cascade={"persist"})
     * @ORM\JoinColumn(name="secteur_id", referencedColumnName="id")
     */
    private $secteur;
    /**
     * @ORM\OneToOne(targetEntity=Utilisateurs::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $responsable;
    /**
     *
     * @ORM\Column(name="notif", type="integer")
     */
    private $notif = 0;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getRaisonsociale()
    {
        return $this->raisonsociale;
    }

    /**
     * @param string $raisonsociale
     */
    public function setRaisonsociale($raisonsociale)
    {
        $this->raisonsociale = $raisonsociale;
    }

    /**
     * @return string
     */
    public function getNomdomaine()
    {
        return $this->nomdomaine;
    }

    /**
     * @param string $nomdomaine
     */
    public function setNomdomaine($nomdomaine)
    {
        $this->nomdomaine = $nomdomaine;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $status
     */
    public function setStatut($status)
    {
        $this->status = $status;
    }
    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->status;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return string
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * @param string $couleur
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    }

    /**
     * @return string
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * @param string $rib
     */
    public function setRib($rib)
    {
        $this->rib = $rib;
    }

    /**
     * @return string
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * @param string $matricule
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;
    }

    /**
     * @return string
     */
    public function getRegistre()
    {
        return $this->registre;
    }

    /**
     * @param string $registre
     */
    public function setRegistre($registre)
    {
        $this->registre = $registre;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }


    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $logo
     *
     * @return boutique
     */
    public function setLogoFile($logo )
    {
        $this->logoFile = $logo;
    }

    /**
     * @return File|null
     */
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * @return mixed
     */
    public function getSecteur()
    {
        return $this->secteur;
    }

    /**
     * @param mixed $secteur
     */
    public function setSecteur(SecteurActivity $secteur)
    {
        $this->secteur = $secteur;
    }

    /**
     * @return mixed
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * @param mixed $responsable
     */
    public function setResponsable(Utilisateurs $responsable)
    {
        $this->responsable = $responsable;
    }

    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }
    /**
     * @return string
     */
    public function getNotif()
    {
        return $this->notif;
    }

    /**
     * @param string $notif
     */
    public function setNotif($notif)
    {
        $this->notif = $notif;
    }

}
