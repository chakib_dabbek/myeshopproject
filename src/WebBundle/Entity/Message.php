<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $message;


    /**
     * @ORM\Column(type="integer")
     */
    private $client_id;


    /**
     * @ORM\Column(type="integer")
     */
    private $grossiste_id;


    /**
     * @ORM\ManyToOne(targetEntity="Utilisateurs", inversedBy="message_client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;


    /**
     * @ORM\ManyToOne(targetEntity="Utilisateurs", inversedBy="message_grossiste")
     * @ORM\JoinColumn(name="grossiste", referencedColumnName="id")
     */
    private $grossiste;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set client_id
     *
     * @param integer $clientId
     * @return Message
     */
    public function setClientId($clientId)
    {
        $this->client_id = $clientId;

        return $this;
    }

    /**
     * Get client_id
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * Set grossiste_id
     *
     * @param integer $grossisteId
     * @return Message
     */
    public function setGrossisteId($grossisteId)
    {
        $this->grossiste_id = $grossisteId;

        return $this;
    }

    /**
     * Get grossiste_id
     *
     * @return integer 
     */
    public function getGrossisteId()
    {
        return $this->grossiste_id;
    }

    /**
     * Set client
     *
     * @param \WebBundle\Entity\Utilisateurs $client
     * @return Message
     */
    public function setClient(\WebBundle\Entity\Utilisateurs $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \WebBundle\Entity\Utilisateurs 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set grossiste
     *
     * @param \WebBundle\Entity\Utilisateurs $grossiste
     * @return Message
     */
    public function setGrossiste(\WebBundle\Entity\Utilisateurs $grossiste = null)
    {
        $this->grossiste = $grossiste;

        return $this;
    }

    /**
     * Get grossiste
     *
     * @return \WebBundle\Entity\Utilisateurs 
     */
    public function getGrossiste()
    {
        return $this->grossiste;
    }
}
