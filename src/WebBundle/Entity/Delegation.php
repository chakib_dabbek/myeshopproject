<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Delegation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;


    /**
     * @ORM\Column(type="string")
     */
    private $code;


    /**
     * @ORM\Column(type="integer")
     */
    private $region_id;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $grossiste_id;


    /**
     * @ORM\Column(type="string")
     */
    private $status;


    /**
     * @ORM\ManyToOne(targetEntity="Utilisateurs", inversedBy="delegations")
     * @ORM\JoinColumn(name="grossiste_id", referencedColumnName="id")
     */
    private $grossiste;


    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="delegations")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Delegation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Delegation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set region_id
     *
     * @param integer $regionId
     * @return Delegation
     */
    public function setRegionId($regionId)
    {
        $this->region_id = $regionId;

        return $this;
    }

    /**
     * Get region_id
     *
     * @return integer 
     */
    public function getRegionId()
    {
        return $this->region_id;
    }

    /**
     * Set grossiste_id
     *
     * @param integer $grossisteId
     * @return Delegation
     */
    public function setGrossisteId($grossisteId)
    {
        $this->grossiste_id = $grossisteId;

        return $this;
    }

    /**
     * Get grossiste_id
     *
     * @return integer 
     */
    public function getGrossisteId()
    {
        return $this->grossiste_id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Delegation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set grossiste
     *
     * @param \WebBundle\Entity\User $grossiste
     * @return Delegation
     */
    public function setGrossiste(\WebBundle\Entity\Utilisateurs $grossiste = null)
    {
        $this->grossiste = $grossiste;

        return $this;
    }

    /**
     * Get grossiste
     *
     * @return \WebBundle\Entity\Utilisateurs
     */
    public function getGrossiste()
    {
        return $this->grossiste;
    }

    /**
     * Set region
     *
     * @param \WebBundle\Entity\Region $region
     * @return Delegation
     */
    public function setRegion(\WebBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \WebBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }
}
