<?php

namespace BoutiqueBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ProduitsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('description', 'Symfony\Component\Form\Extension\Core\Type\TextareaType',
                array('label' => 'Description'))
            ->add('image',FileType::class, array('label' => 'Image', 'required' => FALSE, 'data_class' => NULL))
            ->add('image1',FileType::class, array('label' => 'Image', 'required' => FALSE, 'data_class' => NULL))
            ->add('image2',FileType::class, array('label' => 'Image', 'required' => FALSE, 'data_class' => NULL))
            ->add('prix', 'Symfony\Component\Form\Extension\Core\Type\MoneyType', array(
                'currency' => false,
                'divisor' => 1000,
            ))
            ->add('quantite')
            ->add('nb_achat')
            ->add('is_active')
            ->add('file', VichFileType::class, array(
                'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ))
            ->add('categorie')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebBundle\Entity\Produits'
        ));
    }
}
