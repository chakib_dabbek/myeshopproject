<?php

namespace VetrineBundle\Controller;

use FOS\UserBundle\Mailer\TwigSwiftMailer;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Boutique;
use WebBundle\Entity\Utilisateurs;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
class DefaultController extends Controller
{
    /**
     * @Route("/",name="vetrine")
     */
    public function indexAction()
    {
        return $this->render('VetrineBundle:Default:index.html.twig');
    }
    /**
     * @Route("/newshop",name="create_shop")
     * @Method({"GET", "POST"})
     */
    public function newboutiqueAction(Request $request){
        $boutique =new Boutique();
        $errors=null;
        $form = $this->createForm('BoutiqueBundle\Form\BoutiqueType', $boutique)
            ->add('Commander', 'Symfony\Component\Form\Extension\Core\Type\SubmitType')
            ->remove('nomdomaine')
            ->remove('tel')
            ->remove('adresse')
            ->remove('matricule')
            ->remove('registre')
            ->remove('rib') ;
        $form->handleRequest($request);
        $validator = $this->get('validator');

       if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $user= $em->getRepository('WebBundle:Utilisateurs')->findOneBy( array('email' => $boutique->getEmail()));
           
            if($user){
                $this->addFlash('errorboutique', 'email existant');

 
            }
            else{


            $user= new Utilisateurs();
            $user->setEmail($boutique->getEmail());
            $user->setFirstName($boutique->getEmail());
            $user->setLastName($boutique->getEmail());
            $user->setRoles( array('ROLE_USER') );
            $user->setPlainPassword($request->get('password'));
            $token=new TokenGenerator();
            $mailer=$this->get('fos_user.mailer');
            $user->setConfirmationToken($token->generateToken());
            $mailer->sendConfirmationEmailMessage($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $user= $entityManager->getRepository('WebBundle:Utilisateurs')->findOneBy( array('email' =>$boutique->getEmail() ));

            $boutique->setResponsable($user);
            $entityManager->persist($boutique);
            $entityManager->flush();
            $this->addFlash('success', 'Le boutique crée avec succes');

            /**
             * @var TwigSwiftMailer $mailer
             */

            if ($form->get('Commander')->isClicked()) {
                /* $message = \Swift_Message::newInstance()
                     ->setSubject('Validation du compte')
                     ->setFrom(array('fatma.boussaid@gmail.com' => "Total B2B"))
                     ->setTo($boutique->getEmail())
                     ->setCharset('utf-8')
                     ->setContentType('text/html')
                     ->setBody($this->renderView('BoutiqueBundle:email:validationemail.html.twig', array('boutique' => $boutique)));*/

                return $this->render('VetrineBundle:Default:confirmation.html.twig');
            }
 

            return $this->render('VetrineBundle:Default:confirmation.html.twig');
        }
        }
     $errors=$validator->validate($boutique);
        return $this->render('VetrineBundle:Default:addboutique.html.twig', array(
            'post' => $boutique,
            'form' => $form->createView(),
            'errors'=>$errors
        ));
    }

}
