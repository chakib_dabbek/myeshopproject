<?php

namespace FrontendBundle\Controller;


/**
 * {@inheritDoc}
 */
class SecurityController extends UserController
{

    /**
     * {@inheritDoc}
     */
    public function renderLogin(array $data)
    {

        $requestAttributes = $this->container->get('request')->attributes;
        $authChecker = $this->container->get('security.authorization_checker');

        if ('user_login' === $requestAttributes->get('_route')) {
         //   dump( $this->container->get('security.token_storage')->getToken()->getUser());

                $template = sprintf('FrontendBundle:Default:myboutique.html.twig',array('boutique',$this->boutique->getRaisonsociale()));
            
        } else {
            $template = sprintf('FOSUserBundle:Security:login.html.twig');
        }

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
