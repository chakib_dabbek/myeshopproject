<?php

namespace FrontendBundle\Controller;

use DateTime;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WebBundle\Entity\Commande_Produit;
use WebBundle\Entity\Commandes;
use WebBundle\Entity\Commandes_grossiste;

class CommandesController extends BaseController
{
    public function facture($c,$livraison,$facturation)
    {
        $em = $this->getDoctrine()->getManager();
        $generator = $this->container->get('security.secure_random');
        $session = $this->getRequest()->getSession();
        $panier = $session->get('panier');


        $commande = array();
        $totalHT = 0;


        $produits = $em->getRepository('WebBundle:Produits')->findArray(array_keys($session->get('panier')));

        foreach($produits as $produit)
        {
            $prixHT = ($produit->getPrix() * $panier[$produit->getId()]);
            $totalHT += $prixHT;
            $commande['produit'][$produit->getId()] = array(
                'id'=>$produit->getId(),
                'categorie'=> $produit->getCategorie()->getNom(),
                'reference' => $produit->getNom(),
                'quantite' => $panier[$produit->getId()],
                'prixHT' => round($produit->getPrix(),2),
                'image' => $produit->getPathImage());
            $cp= new Commande_Produit();
            $cp->setProduit($produit);
            $cp->setQuantite($panier[$produit->getId()]);
            $cp->setCommande($c);
            $em->persist($cp);
            $em->flush();
        }

        $commande['livraison'] = array('prenom' => $livraison->getPrenom(),
            'nom' => $livraison->getNom(),
            'telephone' => $livraison->getTelephone(),
            'adresse' => $livraison->getAdresse(),
            'cp' => $livraison->getCp(),
            'ville' => $livraison->getVille(),
            'pays' => $livraison->getPays(),
            'complement' => $livraison->getComplement());

        $commande['facturation'] = array('prenom' => $facturation->getPrenom(),
            'nom' => $facturation->getNom(),
            'telephone' => $facturation->getTelephone(),
            'adresse' => $facturation->getAdresse(),
            'cp' => $facturation->getCp(),
            'ville' => $facturation->getVille(),
            'pays' => $facturation->getPays(),
            'complement' => $facturation->getComplement());


        $commande['prixHT'] = round($totalHT,2);
        $commande['id'] = $c->getId();
        $commande['token'] = bin2hex($generator->nextBytes(20));
        return $commande;
    }



    public function prepareCommandeAction()
    {

        $session = $this->initSession();
        $em = $this->initEntityManager();
        $adresse = $session->get('adresse');
        $facturation = $em->getRepository('WebBundle:UtilisateursAdresses')->find($adresse['facturation']);
        $livraison = $em->getRepository('WebBundle:UtilisateursAdresses')->find($adresse['livraison']);
       // $date = $this->initDate();
       //dump($session->has('commande'));die;
        //var_dump($session->get('commande'));
        //exit();
        if (!$session->has('commande')) {
            //var_dump($session->has('commande'));exit();
            $commande = new Commandes();
            //var_dump($commande);exit();
    }
        else {
            //var_dump($session->get('commande'));exit();
            $commande = $em->getRepository('WebBundle:Commandes')->find($session->get('commande'));
            //var_dump($commande);exit();
        }
        //$commande = new Commandes();
       // var_dump($commande);die();
        $commande->setUtilisateur($this->getUser());
        $commande->setAdressefacturation($facturation);
        $commande->setAdresselivraison($livraison);
        $commande->setReference(md5(uniqid()));
        $commande->setStatusGest(-1);
        $em->persist($commande);
        $em->flush();
        $c=$this->facture($commande,$livraison,$facturation);
        //var_dump($commande);exit();
        if (!$session->has('commande')) { 
            $session->set('commande',$c);
        }
        
        return new Response($c['id']);
    }
    
    /*
     * Cette methode remplace l'api banque.
     */
    /**
     * @Route(path="/mail_test/{id}",  name="send_mail_test")
     */
    public function sendMailTestAction($id)
    {
        $em = $this->initEntityManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);

        //Ici le mail de validation
        $message = \Swift_Message::newInstance()
            ->setSubject('Validation de votre commande')
            ->setFrom(array('b2b.total@gmail.com' => "Total B2B"))
            ->setTo("yosri.mekni.sidratsoft@gmail.com")
            ->setCharset('utf-8')
            ->setContentType('text/html')
            ->setBody($this->renderView('FrontendBundle:SwiftLayout:validationCommande.html.twig',array('commande' => $commande,)), 'text/html');

        $this->get('mailer')->send($message);
       // $result = $mailer->send($message);
        //var_dump($result);die();
        return $this->render('FrontendBundle:SwiftLayout:validationCommande.html.twig'
            ,array('commande' => $commande));
    }
    /**
     * @Route(path="/validation_commande/{id}",  name="validation_commande")
     */
    public function validationCommandeAction($id)
    {
        $em = $this->initEntityManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        
        if (!$commande || $commande->getStatusClient() == 1)
            throw $this->createNotFoundException('La commande n\'existe pas');
        $commande->setStatusClient(1);
        $commande->setDateValidClient(new DateTime('now'));
       $em->merge($commande);
       $em->flush(); 
       $session = $this->initSession(); 
       $commande_valid=$session->get('commande'); 
        $message = \Swift_Message::newInstance()
        ->setSubject('Commande en cours de traitement')
        ->setFrom(array('b2b.total@gmail.com' => "Total B2B"))
        ->setTo($commande->getUtilisateur()->getEmailCanonical())
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setBody($this->renderView('FrontendBundle:SwiftLayout:validationCommande.html.twig',array('commande' => $commande_valid)));
        $this->get('mailer')->send($message);


        $panier = $this->initPanier();
        $adresse = $this->initAdresse();
        $s_commande = $this->initCommande();

        $produits = $this->getProduitsByIds(array_keys($panier));
       
      

        $session->remove('adresse');
        $session->remove('panier');
        $session->remove('commande');
        $session->remove('date');

        //Ici le mail de validation

       

        


        return $this->render('FrontendBundle:Panier:thanks.html.twig',
            array(
                'produits' => $produits,
                'totale' => $commande_valid['prixHT'],
                'panier' => $panier,
                "reference" => $commande->getReference(),
                "message" => null,
                'last_username' => null,
                'error'         => null,
                'csrf_token' => null,
            ));
    }
}
