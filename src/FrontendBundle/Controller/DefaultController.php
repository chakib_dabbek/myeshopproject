<?php

namespace FrontendBundle\Controller;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use WebBundle\Entity\Commande_Produit;
use WebBundle\Entity\ContactUsFormClass;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Categories;
use WebBundle\Entity\Commandes;
use WebBundle\Entity\Produits;
use WebBundle\Entity\Utilisateurs;
use WebBundle\Repository\CategoriesRepository;


class DefaultController extends BaseController
{
    /**
     * @Route("/{boutique}", name="homepage_frontend")
     */
    public function indexAction($boutique)
    {

        $this->initSession();
        $this->initBoutique();
        //$boutique= $this->boutique;
        $em = $this->getDoctrine()->getManager();
        $myboutique = $em->getRepository('WebBundle:Boutique')->findOneBy(array("raisonsociale" => $boutique));
        //$this->session->set('boutique', $myboutique);
        if (($myboutique ) and ($myboutique->getStatut() == true) ) {
           
           if($this->boutique == $myboutique){
            $bout=$myboutique;
            $mesProduits = $em->getRepository('WebBundle:Produits')->findBy(array("boutique" => $bout ,"is_active" => 1));
            $categories = $em->getRepository('WebBundle:Categories')->getCategorieProduit($bout);
           
                return $this->render('FrontendBundle:Default:myboutique.html.twig', array("produits" => $mesProduits,
                "boutique" => $this->boutique, 
                "categories" => $categories,
                "message" => null,
                "panier"=>null,
                'last_username' => null,
                'error'         => null,
                'csrf_token' => null,
            ));
            }
            else{
                $session = $this->initSession();
                $panier = $this->initPanier();
                $b= $this->initBoutique();
                $this->session->set('boutique', $myboutique);
                $this->session->set('panier',$panier);
                $bout=$myboutique;
                $mesProduits = $em->getRepository('WebBundle:Produits')->findBy(array("boutique" => $bout ,"is_active" => 1));
                $categories = $em->getRepository('WebBundle:Categories')->getCategorieProduit($bout);
                return $this->render('FrontendBundle:Default:myboutique.html.twig', array("produits" => $mesProduits,
                "boutique" => $this->boutique, 
                "categories" => $categories,
                "message" => null,
                "panier"=>null,
                'last_username' => null,
                'error'         => null,
                'csrf_token' => null,
            ));
    
            }
            
              
        }
        else{
            return $this->render('VetrineBundle:Default:index.html.twig');

        }
        
       
       
          
        
    }
    /**
     * @Route("/", name="client_boutique ")
     */
    public function boutiqueAction(Request $request, $boutique, $message=null)
    {
  
     //   dump($panier);die;
        $em = $this->getDoctrine()->getManager();
        $myboutique = $em->getRepository('WebBundle:Boutique')->findOneBy(array("raisonsociale" => $boutique));
        if ($myboutique) {
          //  dump($boutique);
            $mesProduits = $em->getRepository('WebBundle:Produits')->findBy(array("boutique" => $myboutique, "is_active" => 1));
            $categories = $em->getRepository('WebBundle:Categories')->getCategorieProduit($myboutique);
           
            return $this->render('FrontendBundle:Default:myboutique.html.twig', array("produits" => $mesProduits,
            "boutique" => $myboutique, 
            "categories" => $categories,
            "message" => $message,
            "panier" => $panier,
            'last_username' => null,
            'error'         => null,
            'csrf_token' => null,
        ));
        } else {
            /* if ($boutique=="backend") {
                $this->redirectToRoute("admin_login");
            } */
           
            return $this->render('VetrineBundle:Default:index.html.twig');
        }


        //return $this->render('FrontendBundle:Produit:index.html.twig', array("produits"=>$produits));
    }

     /**
     * @Route("categorie/{categorie}/{boutiqueid}", name="client_categorie")
     */
    public function categorieAction(Request $request, $categorie, $boutiqueid)
    {
        // get boutique
        $em = $this->getDoctrine()->getManager();
        $myboutique = $em->getRepository('WebBundle:Boutique')->findOneBy(array("id" => $boutiqueid));
        if ($myboutique) {   
            $mesProduits = $em->getRepository('WebBundle:Produits')->findBy(array("boutique" => $myboutique, "categorie"=>$categorie, "is_active" => 1));
            $categories = $em->getRepository('WebBundle:Categories')->getCategorieProduit($myboutique);
            return $this->render('FrontendBundle:Default:myboutique.html.twig', array(
                "produits" => $mesProduits,
                "boutique" => $myboutique,
                "categories" => $categories,
                "message" => null,
                'last_username' => null,
                'error'         => null,
                'csrf_token' => null,

            ));
        } else {
            return $this->render('FrontendBundle:Default:index.html.twig');
        }


        //return $this->render('FrontendBundle:Produit:index.html.twig', array("produits"=>$produits));
    }
    /**
     * @Route("client/inscription/{boutique}", name="inscription_client")
     */
    public function inscriptionAction(Request $request, $boutique)
    {
        $em = $this->getDoctrine()->getManager();
        $verif=$em->getRepository('WebBundle:Utilisateurs')->findOneBy(array("email"=>$request->request->get('email')));
        if ($verif) {
            $msg="problème d'inscription: email existe déjà";
        } else {
            $inscription=new Utilisateurs();
            $inscription->setFirstName($request->request->get('nom'));
            $inscription->setLastName($request->request->get('prenom'));
            $inscription->setUsername($request->request->get('email'));
            $inscription->setEmail($request->request->get('email'));
            $inscription->setEnabled(true);
            $inscription->setRoles( array('ROLE_CLIENT') );
            $inscription->setEmailCanonical($request->request->get('email'));
            $inscription->setPlainPassword($request->request->get('password'));
            $em->persist($inscription);
            $em->flush();
            $msg="inscription avec succée";
        }
        return $this->redirectToRoute("homepage_frontend", array("boutique"=>$boutique));
      
       
    }

    /**
     * @Route("/contact", name="contact_us")
     */

    public function contactAction(Request $request)
    {
        // create a task and give it some dummy data for this example
        $contactEntity = new ContactUsFormClass();


        $form = $this->createFormBuilder($contactEntity)
            ->add(
                'civilite',
                ChoiceType::class,
                array(
                    'choices' => array(
                        'M.' => 'Monsieur',
                        'Mme.' => 'Madame'
                    ),
                    'choices_as_values' => true,
                    'multiple' => false,
                    'expanded' => true
                )
            )
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('ville', TextType::class)
            ->add('zone', TextType::class)
            ->add('email', EmailType::class, array('required' => true))
            ->add('tel', TextType::class, array('required' => true))
            ->add('object', ChoiceType::class, array(
                'required' => true,
                'multiple' => true,
                'choices' => array(
                    'Prix' => 'Prix',
                    'Réclamation' => 'Réclamation',
                    'Assistance technique' => 'Assistance technique',
                    'Formation' => 'Formation',
                    'Autre' => 'Autre',
                ),
            ))
            ->add('sujet', TextType::class, array('required' => true))
            ->add('message', TextareaType::class)
            ->add('captcha', CaptchaType::class, array(
                'reload' => true,
                'as_url' => true,
            ))
            ->getForm();
        $done = 0;
        if ($this->get('request')->getMethod() == 'POST') {
            $form->handleRequest($this->getRequest());
            //var_dump($form->getData());die();
            if ($form->isValid()) {
                $sexe = $contactEntity->getCivilite();
                $nom = $contactEntity->getNom();
                $prenom = $contactEntity->getPrenom();
                $ville = $contactEntity->getVille();
                $zone = $contactEntity->getZone();
                $email = $contactEntity->getEmail();
                $tel = $contactEntity->getTel();
                $objet = $contactEntity->getObject();
                $adresse = $contactEntity->getAdresse();
                $sujet = $contactEntity->getSujet();
                $message = $contactEntity->getMessage();


                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact utilisateur')
                    ->setFrom(array($email => "Utilisateur Total B2B"))
                    ->setTo("b2b.total@gmail.com")
                    ->setCharset('utf-8')
                    ->setContentType('text/html')
                    ->setBody($this->renderView('FrontendBundle:SwiftLayout:contactContent.html.twig', array(
                        'sexe' => $sexe,
                        'nom' => $nom,
                        'prenom' => $prenom,
                        'adresse' => $adresse,
                        'ville' => $ville,
                        'zone' => $zone,
                        'email' => $email,
                        'tel' => $tel,
                        'objet' => $objet,
                        'sujet' => $sujet,
                        'msg' => $message
                    )), 'text/html');

                $this->get('mailer')->send($message);
                $done = 1;
            }
        }
        $request = $this->container->get('request');
        $routeName = $request->get('_route');
        return $this->render('FrontendBundle:Default:contact.html.twig', array('cp' => $routeName, 'form' => $form->createView(), 'done' => $done));
    }


    /**
     * @Route("/cart")
     */
    public function cartAction()
    {
        return $this->render('@Frontend/Default/cart.html.twig');
    }



    /**
     * @Route("/produits", name="list_produits")
     */
    public function produitsAction()
    {
        return $this->render('FrontendBundle:Default:produits.html.twig');
    }

    /**
     * @Route("/detailsproduit", name="details_produit")
     */
    public function detailsproduitAction()
    {
        return $this->render('FrontendBundle:Default:detailsproduit.html.twig');
    }

   
  
}
