<?php

namespace FrontendBundle\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FrontendBundle\Controller\BaseController;
use FrontendBundle\Form\UtilisateursAdressesType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use WebBundle\Entity\UtilisateursAdresses;
use WebBundle\Form\UtilisateursType;

/**
 * @Route("/profile")
 */
class UserController extends BaseController
{

    /**
     * @Route(path="/", name="compte")
     */
    public function indexAction(Request $request)
    {
        return $this->render('FrontendBundle:Profile:compte.html.twig',array(
               "message" => null,
                'last_username' => null,
                'error'         => null,
                'csrf_token' => null,
        ));
    }

    /**
     * @Route(path="/facture", name="facture")
     */
    public function factureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $factures = $em->getRepository('WebBundle:Commandes')->byFacture($this->getUser());

        return $this->render('FrontendBundle:Profile:facture.html.twig',array('factures' => $factures));
    }

    /**
     * @Route(path="/facture/{id}", name="facture_detail")
     */
    public function factureDetailAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $facture = $em->getRepository('WebBundle:Commandes')->byFacture($this->getUser(), $id);

        return $this->render('FrontendBundle:Profile:factureDetail.html.twig',array('facture' => $facture));
    }

    /**
     * @Route(path="/adresse",  name="profile_adresse")
     */
    public function adresseAction(Request $request)
    {

        $em = $this->initEntityManager();
        $utilisateur = $this->getUser();
        $entity = new UtilisateursAdresses();
        $form = $this->createForm(new UtilisateursAdressesType($em), $entity);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->handleRequest($this->getRequest());
            //var_dump($form->getData());die();
           // if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setUtilisateur($utilisateur);

                $delegation = $em->getRepository('WebBundle:Delegation')->findOneBy(array('code' => $entity->getCp()));
                //var_dump($delegation);die();
                if ($delegation) {
                    $entity->setPays($delegation->getRegion()->getName());
                    $entity->setVille($delegation->getName());
                }

                $em->persist($entity);
                $em->flush();

                
                //return $this->redirect($this->generateUrl('compte'));
            //}
        }

        return $this->render('FrontendBundle:Profile:adresse.html.twig', array('utilisateur' => $utilisateur,
            'form' => $form->createView(),
            "message" => null,
            'last_username' => null,
            'error'         => null,
            'csrf_token' => null,
            ));
    }
     /**
     * @Route(path="/edit",  name="profile_edit")
     */
    public function informationAction(Request $request)
    {

        $em = $this->initEntityManager();
        $utilisateur = $this->getUser();
       
        $form = $this->createForm(new UtilisateursType($em), $utilisateur);
        if ($this->get('request')->getMethod() == 'POST') {
            $form->handleRequest($this->getRequest());
            //var_dump($form->getData());die();
           // if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $nv_pass=$this->get('security.password_encoder')->encodePassword($utilisateur,$utilisateur->getPassword() );
               if($this->getUser()->getPassword() == $nv_pass){
                $utilisateur->setPassword($nv_pass);
                $em->persist($utilisateur);
                $em->flush();


               }
                $this->get('session')->getFlashBag()->add('editinfo','mot de passe erroné');
                //return $this->redirect($this->generateUrl('compte'));
            //}
        }

        return $this->render('FrontendBundle:Profile:edit.html.twig', array('utilisateur' => $utilisateur,
            'form' => $form->createView(),
            "message" => null,
            'last_username' => null,
            'error'         => null,
            'csrf_token' => null,
            ));
    }
    /**
    * @Route(path="/editpass",  name="password_edit")
    */
    public function changePasswordAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->container->get('fos_user.change_password.form');
        $formHandler = $this->container->get('fos_user.change_password.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->get('session')->getFlashBag()->add('motdepass','mot de passe changé  avec succès');
            return $this->render('FrontendBundle:ChangePassword:changePassword.html.twig', array(
            'form' => $form->createView(),
            "message" => null,
            'last_username' => null,
            'error'         => null,
            'csrf_token' => null,
            ));
        }

        return $this->render('FrontendBundle:ChangePassword:changePassword.html.twig', array(
        'form' => $form->createView(),
        "message" => null,
        'last_username' => null,
        'error'         => null,
        'csrf_token' => null,
        ));
    }
    /**
     * @Route(path="/facturepdf/{id}",  name="facturepdf")
     */
    public function facturesPDFAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $facture = $em->getRepository('WebBundle:Commandes')->findOneBy(array('utilisateur' => $this->getUser(),
            'valider' => 1,
            'id' => $id));

        if (!$facture) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenue');
            return $this->redirect($this->generateUrl('facutre'));
        }

        $this->container->get('setNewFacture')->facture($facture)->Output('Facture.pdf');

        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }
    /**
     * @Route("logout/{boutique}", name="logout_user")
     */

    public function logoutAction($boutique)
    {
        
        $session = $this->initSession();
        $date = $this->initDate();
        $commande = $this->initCommande();
        $addresse = $this->initAdresse();

        $this->get('security.context')->setToken(null);
        $this->get('request')->getSession()->invalidate();

        $session->set("commande",$commande);
        $session->set("adresse",$addresse);
        $session->set("date",$date);

        return $this->redirectToRoute('homepage_frontend',array('boutique'=> $this->boutique->getRaisonsociale()));
    }

    public function csrfUserFormAction()
    {
        $csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');


        return $this->render('FrontendBundle:User:csrfUser.html.twig', array("csrf_token" => $csrfToken));
    }





}
?>