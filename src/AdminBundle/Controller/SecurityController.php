<?php

namespace AdminBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

/**
 * {@inheritDoc}
 */
class SecurityController extends BaseController
{

    /**
     * {@inheritDoc}
     */
    public function renderLogin(array $data)
    {
        $requestAttributes = $this->container->get('request')->attributes;
        $authChecker = $this->container->get('security.authorization_checker');
        $this->container->get('session')->getFlashBag()->clear();
        if ('admin_login' === $requestAttributes->get('_route')) {
            if ($authChecker->isGranted('ROLE_SUPER_ADMIN')) {
                $template = sprintf('AdminBundle:Default:index.html.twig');
            } 
            else {
                $this->container->get('security.context')->setToken(null);
                $this->container->get('request')->getSession()->invalidate();
                $this->container->get('session')->getFlashBag()->add('sessionadmin', 'Email ou mot de passe incorrect');
                $template = sprintf('AdminBundle:Security:login.html.twig');
            }
        } else {
            $template = sprintf('FOSUserBundle:Security:login.html.twig');
        }
        return $this->container->get('templating')->renderResponse($template, $data);
    }
}
