<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="dashadmin")
     */
    public function indexAction()
    {                 
        $this->get('session')->getFlashBag()->clear();
        $em = $this->getDoctrine()->getManager();
        $nbBoutiques=count($em->getRepository('WebBundle:Boutique')->findAll());
            $nbCategories=count($em->getRepository('WebBundle:Categories')->findAll());
           $nbSecteurs=count($em->getRepository('WebBundle:SecteurActivity')->findAll());
           $nbUser=count($em->getRepository('WebBundle:Utilisateurs')->findAll());
           $boutiques = $em->getRepository('WebBundle:Boutique')->findBy(array('notif' => 0));
           $this->getRequest()->getSession()->set('notif', $boutiques);
           $this->getRequest()->getSession()->set('nbnotif', count($boutiques));
            return $this->render('AdminBundle:Default:index.html.twig', array( 
                "boutiques" => $nbBoutiques, 
                "categories" => $nbCategories,
               "secteurs" => $nbSecteurs,
                "users" => $nbUser
            ));
    }
    /**
    * @Route("/notif", name="notif_backend")
    */
    public function notification(){
        $boutiques=$this->getRequest()->getSession()->get('notif');
        if($boutiques){
        foreach($boutiques as $boutique){
            $em = $this->getDoctrine()->getManager();
            $boutique->setNotif(1);
            $em->merge($boutique);
        }
        $em->flush();
        $this->getRequest()->getSession()->remove('notif');
        $this->getRequest()->getSession()->set('nbnotif',0);
    }

        return $this->render('AdminBundle:Default:notifications.html.twig',array('boutiques'=> $boutiques));
    }
}
